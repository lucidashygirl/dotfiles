# i3config

## Introduction

Hai I'm Luci, this is my personal configuration for i3, but I have it open source, because why not? It's not the most polished, but you're free to use it however you like (Within the GPL)

## Dependencies 

This configuration requires the following applications:

- feh
    - checks for file at '~/Documents/wallpaper/default_wallpaper.*'
- dmenu 
    - My version of dmenu is the intended version, you can find it at  
    https://gitlab.com/lucidashygirl/dmenu
- polybar
    - My configuration for polybar can be found at  
    https://gitlab.com/lucidashygirl/

