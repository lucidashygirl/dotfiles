function cdc --wraps='cd && clear' --description 'alias cdc=cd && clear'
  cd && clear $argv
        
end
