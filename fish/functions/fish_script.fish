function fish_script
    cd ~/.config/fish/functions
    touch $argv.fish
    vim $argv.fish
end
