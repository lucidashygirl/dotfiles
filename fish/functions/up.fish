function up --wraps='sudo pacman -Syu && yay -Syu' --description 'alias up=sudo pacman -Syu && yay -Syu'
  sudo pacman -Syu && yay -Syu $argv
        
end
