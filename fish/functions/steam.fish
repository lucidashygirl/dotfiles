function steam --wraps='flatpak run com.valvesoftware.Steam' --description 'alias steam=flatpak run com.valvesoftware.Steam'
  flatpak run com.valvesoftware.Steam $argv
        
end
